#!/usr/bin/python

# Mission Control Center
# A tool for sysadmins to manage your remote servers
# Version: 0.1.1 (alpha)
# If you want to report bugs or help development: https://bitbucket.org/pedur/mcc

# Copyright Peter den Hartog (2012)

# This file is part of MCC.

# MCC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MCC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with MCC.  If not, see <http://www.gnu.org/licenses/>.

# Import the modules we need
import psycopg2
import os
import subprocess
import sys

# Function to list the servers
def listservers():
	cur.execute("SELECT * FROM servers;")
	output = cur.fetchall()
	if not output:
		print "No servers found, please add a server"
		print "Returning to the main menu"
		mainmenu()
	else:
		print "These are all your servers:"
		for x in output:
			print x[0]
			
# Function to clear the screen
def clearscreen():
	if os.name == "posix":
	# Unix/Linux/MacOS/BSD/etc
    		os.system('clear')
	elif os.name in ("nt", "dos", "ce"):
	# DOS/Windows
    		os.system('CLS')
	else:
	# Fallback for other operating systems.
    		print '\n' * numlines

# Function to query server
def queryserver(servername):
	cur.execute("SELECT * FROM servers WHERE hostname=\'"+servername+"\';")
	output2 = cur.fetchone()
	if not output2:
		print "No result found"
		print "Returning to main menu"
		mainmenu()
	else:
		print "Hostname:", output2[0]
		print "IP:", output2[1]
		print "Username:", output2[2]
		print "Description:", output2[3]
	
# Function to list server info
def selectserver():
	userinput = raw_input ("Please type the server name: ")
	print "You have selected:", userinput 
	queryserver(userinput)
	return userinput

# Function to add server
def addserver():
	print "Adding server, please provide the following info:"
	userinput = raw_input ("hostname: ")
	userinput2 = raw_input ("IP: ")
	userinput3 = raw_input ("Username: ")
	userinput4 = raw_input ("Description: ")
	cur.execute("INSERT INTO servers(hostname, ip, un, description) VALUES('"+userinput+"', '"+userinput2+"', '"+userinput3+"', '"+userinput4+"');")	
	conn.commit()

# Function to edit server
def editserver():
	print "edit server"
	print "function not done, will come in later version"
	mainmenu()

# Function to delete current selected server
def deleteserver():
	print "Which server do you want to delete:"
	listservers()
	userinput = raw_input ("Hostname: ")
	userinput2 = raw_input ("Are you sure you want to remove "+userinput+"? (yes/no/exit) ")
	if userinput2 == 'yes':
		cur.execute("DELETE FROM servers WHERE hostname = '"+userinput+"';")
		conn.commit()
		print (""+userinput+" succesfully removed.")
	elif userinput2 == 'no':
		print (""+userinput+" not removed.")
	elif userinput2 == 'exit':
		listservers()
	else:
		print "Wrong input, use yes, no or exit.. Going back home"

# Function for searching servers
def searchserver():
	print "Please enter the hostname of the server you want to find:"
	userinput = raw_input ("Hostname: ")
	queryserver(userinput)

# Function for the mainmenu
def mainmenu(): # Main menu
	print "= MAIN MENU ="
	print "Please pick an option:"
	print "  - select server"
	print "  - edit server"
	print "  - add new server"
	print "  - delete server"
	print "  - list servers"
	print "  - search server"
	print "  - exit"
	userinput = raw_input ("What do you want to do? ")
	
	if userinput == 'select server':
		listservers()
		selectedserver = selectserver()
		servermenu(selectedserver)

	elif userinput == 'edit server':
		editserver()
		mainmenu()

	elif userinput == 'add new server':
		addserver()
		mainmenu()
		
	elif userinput == 'delete server':
		deleteserver()
		mainmenu()
		
	elif userinput == 'list servers':
		listservers()
		mainmenu()
			
	elif userinput == 'search server':
		searchserver()
		mainmenu()

	elif userinput == 'exit':
		sys.exit()

	else: # Go back to the start of the main menu 
		mainmenu()

# Function to show the server menu
def servermenu(server):
		print "Please pick an option:"
		print "  - SSH connect"
		print "  - Go to main menu"
		userinput = raw_input ("What do you want to do? ")
		if userinput == 'SSH connect': # SSH connector for servers
			print "ssh connection"
			cur.execute ("SELECT ip,un FROM servers WHERE hostname=\'"+server+"\';")
			cred = cur.fetchone()
    			subprocess.call(["ssh", cred[1]+"@"+cred[0]])
			mainmenu()

		else: # Go home
			mainmenu()	

# Connect to DB
conn = psycopg2.connect("dbname=mcc user=mcc")
cur = conn.cursor()
 # Calls the function to clear the screen
clearscreen()

# Call the main menu
print "Welcome to MCC!"
mainmenu()

# Cleanup database connection
cur.close()
conn.close()
